package com.c0d3in3.lecture2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var currentToast : Toast? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        generateButton.setOnClickListener {
            val generatedNumber = numberGenerator()
            currentToast?.cancel()
            currentToast = if(generatedNumber) Toast.makeText(this, "This number is even", Toast.LENGTH_SHORT)
            else Toast.makeText(this, "This number is odd", Toast.LENGTH_SHORT)
            currentToast?.show()
        }
    }

    private fun numberGenerator() : Boolean{
        val randomNumber = (1..100).random()
        updateUIObject(generatedNumberTextView, randomNumber.toString())
        return randomNumber %2 == 0
    }

    private fun updateUIObject(obj: TextView, text: String){
        obj.text = text
    }
}
